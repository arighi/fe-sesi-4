# FE Sesi 4

## Introduction

- how css works
    - dom
- css syntax
    - declaration = properties + : + values + ;
    - declaration blocks = declaration + declaration + …
    - rule = selector + { + declaration block + }
    - comments = /* … */
    - shorthand properties
- selectors ([ref](https://www.w3schools.com/cssref/css_selectors.asp))
    - simple selectors
    - attribute selectors
    - pseudo-classes
    - pseudo-elements
    - combinators
    - multiple selectors
- value & units
    - numeric
    - percentages
    - colors
    - functions
- cascade
    - source order
    - specificity
    - importance
- inheritance:
    - inherit
    - initial
    - unset
- box model
  - box properties:
    - `width` & `height`
    - `padding`
    - `margin`
    - `border`
  - types:
    - block
    - inline
    - inline-block
- debugging

## Styling text

- fundamental text & font styling
  - fonts:
    - `color`
    - font families & font stacks
    - `font-style`
    - `font-weight`
    - `text-transform`
    - `text-decoration`
    - `text-shadow`
    - etc
  - layout:
    - `text-align`
    - `line-height`
    - `letter-spacing` & `word-spacing`
    - etc
- styling lists
  - `list-style` (type, position, image)
  - counting (start, reversed, value
- styling links
  - link (a)
  - visited
  - hover
  - focus
  - active
- web fonts

## Styling Boxes

- box model recap
  - recap
  - `box-sizing`
  - display types
- backgrounds
  - `background-color`
  - `background-image`
  - `background-position`
  - `background-repeat`
  - `background-size`
  - `background-attachment` ([example](https://mdn.github.io/learning-area/css/styling-boxes/backgrounds/background-attachment.html))
- borders
  - `border-top`, `border-right`, `border-bottom`, `border-left`
  - `border-width`, `border-style`, `border-color`
  - `border-top-width`, `border-right-style`, etc
  - `border-radius`
- advance box effects
  - `box-shadow`
  - `filter`
  - `blend-mode` ([example](https://mdn.github.io/learning-area/css/styling-boxes/advanced_box_effects/blend-modes.html))
  - `-webkit-background-clip`

## CSS Layout
- normal flow
- flexbox
- grids
- floats
- positioning
- multiple column layout
- legacy layout methods
- supporting older browsers

## Reference

Full reference: https://developer.mozilla.org/en-US/docs/Learn/CSS
